﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GildedRose.Console.Processors
{
    public class ConjuredItemProcessor : ItemProcessor {
        public ConjuredItemProcessor(Item item) : base(item)
        {}

        protected override void ChangeQuantity()
        {
            if (IsExpired())
            {
                if (item.Quality > 0)
                {
                    item.Quality -= 4;
                }
            }
            else
            {
                if (item.Quality > 0)
                {
                    item.Quality -= 2;
                }
            }
        }
    }
}
