﻿using GildedRose.Console.Processors;
using System.Collections.Generic;

namespace GildedRose.Console
{
    public class Program
    {
        public const string AGED_BRIE = "Aged Brie";
        public const string BACKSTAGE_PASSES_TO_A_TAFKAL80_ETC_CONCERT = "Backstage passes to a TAFKAL80ETC concert";
        public const string SULFURAS_HAND_OF_RAGNAROS = "Sulfuras, Hand of Ragnaros";
        public const string DEXTERITY_VEST = "+5 Dexterity Vest";
        public const string ELIXIR_OF_THE_MONGOOSE = "Elixir of the Mongoose";
        public const string CONJURED_MANA_CAKE = "Conjured Mana Cake";

        public IList<Item> Items;
        static void Main(string[] args)
        {
            System.Console.WriteLine("OMGHAI!");

            var app = new Program()
                          {
                              Items = new List<Item>
                                          {
                                              new Item {Name = DEXTERITY_VEST, SellIn = 10, Quality = 20},
                                              new Item {Name = AGED_BRIE, SellIn = 2, Quality = 0},
                                              new Item {Name = ELIXIR_OF_THE_MONGOOSE, SellIn = 5, Quality = 7},
                                              new Item {Name = SULFURAS_HAND_OF_RAGNAROS, SellIn = 0, Quality = 80},
                                              new Item
                                                  {
                                                      Name = BACKSTAGE_PASSES_TO_A_TAFKAL80_ETC_CONCERT,
                                                      SellIn = 15,
                                                      Quality = 20
                                                  },
                                              new Item {Name = CONJURED_MANA_CAKE, SellIn = 3, Quality = 6}
                                          }

                          };

            app.UpdateQuality();

            System.Console.ReadKey();

        }

        public void UpdateQuality()
        {
            foreach (Item item in Items)
            {
                GetItemProcessor(item).Process();
            }
        }

        private ItemProcessor GetItemProcessor(Item item)
        {
            if (item.Name.Equals(BACKSTAGE_PASSES_TO_A_TAFKAL80_ETC_CONCERT))
            {
                return new BackstagePassProcessor(item);
            }
            else if (item.Name.Equals(AGED_BRIE))
            {
                return new AgingProcessor(item);
            }
            else if (item.Name.Equals(SULFURAS_HAND_OF_RAGNAROS))
            {
                return new LegendaryItemProcessor(item);
            }
            else if (item.Name.Equals(CONJURED_MANA_CAKE))
            {
                return new ConjuredItemProcessor(item);
            }
            else
            {
                return new ItemProcessor(item);
            }
        }

    }
}
