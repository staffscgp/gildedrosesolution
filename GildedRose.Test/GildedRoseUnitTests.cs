﻿using GildedRose.Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Collections.Generic;

namespace GildedRose.Test
{
    [TestClass]
    public class GildedRoseUnitTests
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod]
        public void TestSellInDecrease()
        {
            var item = new Item { Name = "standard item", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(0, item.SellIn);
        }

        [TestMethod]
        public void TestQualityDecrease()
        {
            var item = new Item { Name = "standard item", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestSellInPassedQualityDecreaseDoubles()
        {
            var item = new Item { Name = "standard item", SellIn = 0, Quality = 2 }.UpdateQuality();
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestQualityNeverNegative()
        {
            var item = new Item { Name = "standard item", SellIn = 1, Quality = 0 }.UpdateQuality();
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestLegendaryNoQualityDecrease()
        {
            var item = new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(1, item.Quality);
        }

        [TestMethod]
        public void TestLegendaryNoSellInDecrease()
        {
            var item = new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 1, Quality = 0 }.UpdateQuality();
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestAgedQualityIncrease()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(2, item.Quality);
        }

        [TestMethod]
        public void TestAgedQualityNeverAboveFifty()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 1, Quality = 50 }.UpdateQuality();
            Assert.AreEqual(50, item.Quality);
        }

        [TestMethod]
        public void TestAgedSellInDecrease()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(0, item.SellIn);
        }

        [TestMethod]
        public void TestAgedSellInZeroQualityDoubleIncrease()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 0, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(3, item.Quality);
        }

        [TestMethod]
        public void TestEventQualityIncrease()
        {
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 11, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(2, item.Quality);
        }

        [TestMethod]
        [DeploymentItem("GildedRose.Test\\TestEventTenOrLessDaysQualityIncreaseByTwo.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                           "|DataDirectory|\\TestEventTenOrLessDaysQualityIncreaseByTwo.xml",
                           "Row",
                            DataAccessMethod.Sequential)]
        public void TestEventTenOrLessDaysQualityIncreaseByTwo()
        {
            int sellIn = Convert.ToInt32(TestContext.DataRow["daysLeft"]);
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = sellIn, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(3, item.Quality);
        }

        [TestMethod]
        [DeploymentItem("GildedRose.Test\\TestEventFiveOrLessDaysQualityIncreaseByThree.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                           "|DataDirectory|\\TestEventFiveOrLessDaysQualityIncreaseByThree.xml",
                           "Row",
                            DataAccessMethod.Sequential)]
        public void TestEventFiveOrLessDaysQualityIncreaseByThree()
        {
            int sellIn = Convert.ToInt32(TestContext.DataRow["daysLeft"]);
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = sellIn, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(4, item.Quality);
        }

        [TestMethod]
        public void TestEventSellInPassedQualityEqualZero()
        {
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(0, item.Quality);
        }

        [TestMethod]
        public void TestEventSellInDecrease()
        {
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 1, Quality = 1 }.UpdateQuality();
            Assert.AreEqual(0, item.SellIn);
        }
    }

    public static class TestHelper
    {
        public static Item UpdateQuality(this Item item)
        {
            var program = new Program { Items = new List<Item> { item } };
            program.UpdateQuality();
            return item;
        }
    }
}
